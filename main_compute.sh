#!/bin/bash

# --- Set path
# ffpath=.../
export ffpath="$HOME/Soft/FreeFem-412/install/bin"
export off=" -nw -ne -v 0 -glut $ffpath/ffglut "
export xff="$ffpath/FreeFem++ $off "

# --- Settings
# Profile
export profile=0.5

# Windkessel
export wdksl=" -rp 6.26e-6 -rd 2e-6 ";                     # Default
# export wdksl=" -rp 6.26e-6 -rd 2e-6 -cp 3e5 -lp 0.0 ";     # RCR
# export wdksl=" -rp 6.26e-6 -rd 2e-6 -cp 3e5 -lp 8.26e-7 "; # RCRL

# Path
export snappath="./results/default/"; # Default
# export snappath="./results/RCR/";     # RCR
# export snappath="./results/RCRL/";    # RCRL
mkdir -p ${snappath};

# --- Steady Stokes init
$xff SteadyStokes.edp \
	-input ./inflow/inflow.dat \
	-profile ${profile} \
	-out ${snappath} \
	${wdksl} \
	-savevtu ;

# --- Time trajectory
$xff UnsteadyStokes.edp \
	-input ./inflow/inflow.dat \
	-ssinit ${snappath}SteadyStokes.dat \
	-profile ${profile} \
	-out ${snappath} \
	${wdksl} \
	-savevtu ;
