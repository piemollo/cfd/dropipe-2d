import sys
import numpy as np
import pylab as pl

pl.rcParams.update({
    "text.usetex": True,
    "font.family": "mathplazo"
})

''' Create profile variation '''

def main():
    profile_data = np.loadtxt('./InputBoundSettings.dat')
    sort_idx     = np.argsort(profile_data[:,2]);

    B_idx = profile_data[sort_idx,0];
    Bx    = profile_data[sort_idx,1];
    By    = profile_data[sort_idx,2];
    Wx    = profile_data[sort_idx,3];
    Wy    = profile_data[sort_idx,4];

    # --- profile forms
    BBy = np.linspace(np.min(By), np.max(By), int(1e5));
    poi = lambda y : y*(6-y)/36.;
    gau = lambda y,m,s : (1/np.sqrt(2*np.pi))*np.exp(-0.5*((y-m)**2)/(s**2));
    sig = 1.2;

    # Multiply with Gaussian
    Bpoi  = poi(By);
    Bgaul = gau(By, 1.5, sig)*Bpoi;
    Bgaur = gau(By, 4.5, sig)*Bpoi;

    # Normalizer
    ds = np.dot(Bpoi,Wx) + np.dot(Bpoi,Wy);
    Bgaul /= - np.dot(Bgaul,Wx) - np.dot(Bgaul,Wy);
    Bgaur /= - np.dot(Bgaur,Wx) - np.dot(Bgaur,Wy);

    # --- Display
    fg = pl.figure(1, figsize=(5,3), constrained_layout=True)

    for i in range(3):
        w = np.random.rand(2,1);
        w = w/np.linalg.norm(w,1);
        pl.plot(By, w[0]*Bgaul + w[1]*Bgaur, ':b', linewidth=1)

    pl.plot(By, Bpoi, linewidth=2.5, label='Poiseuille', color=[0.4660,0.6740,0.1880])
    pl.plot(By, Bgaul, '--r', linewidth=1.5, label='P*G left/right')
    pl.plot(By, Bgaur, '--r', linewidth=1.5)
    
    pl.title("Input velocity profile")
    pl.xlabel("Section [$mm$]")
    pl.ylabel("Normalized velocity [$mm.s^{-1}$]")

    pl.grid(True, linestyle=':')
    pl.legend()
    #pl.show()
    fg.savefig("../assets/input_profile.pdf")

    # --- Save profiles
    with open('InputProfile.dat', 'w') as f:
        for i in range(len(B_idx)):
            f.write( "{:d} ".format( int(B_idx[i])) )
            f.write( "{:f} ".format( Bgaul[i]) )
            f.write( "{:f} \n".format(Bgaur[i]) )

    return 0

if __name__ == '__main__':
    main()

