# Numerical simulation: 2D Vascular pipe 
Project by Pierre Mollo (p.m.mollo@tue.nl).
Create: 2023-07-26, last revision:  2023-08-31.

----

## Description

This project is a full simulation framework for CFD in a 2D
straight rigid pipe and includes 0D Windkessel coupling.

To work this project required local 
[`FreeFEM`](https://freefem.org/) installation.


## How to use it

Use the following commands to download the main project and 
mandatory sub-modules
````bash
git clone --recurse-submodules https://gitlab.com/piemollo/cfd/dropipe-2d

````
The path to FreeFEM binaries must be set in shell scripts 
`main_initialize.sh` and `main_compute.sh`,
then the commands 
````bash
chmod +x ./main_initialize.sh
chmod +x ./main_compute.sh
./main_initialize.sh
````
allows to set up the project.

Simulation settings such as the label of results and Windkessel
parameters are prescribed directly in the script `main_compute.sh`
and the simulation can be ran using the commands
````bash
./main_compute.sh
````

By default, results are stored in a sub-folder of `results/`, 
named after the prescribed label.
